/**
 * @ngdoc overview
 * @name transcend.export
 * @description
 # Job Module
 The "Job" module provides a set of functionality to view and interact with jobs that have been ran. Jobs are typically
 long running processes that process data, export images, etc.

 ## Installation
 The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
 commands to install this package:

 ```
 bower install https://bitbucket.org/transcend/bower-job.git
 ```

 Other options:

 * Download the code at [http://code.tsstools.com/bower-job/get/master.zip](http://code.tsstools.com/bower-job/get/master.zip)
 * View the repository at [http://code.tsstools.com/bower-job](http://code.tsstools.com/bower-job)

 When you have pulled down the code and included the files (and dependency files), simply add the module as a
 dependency to your application:

 ```
 angular.module('myModule', ['transcend.job']);
 ```

 ## To Do
 - Implement change job expiration.
 */
/**
 * @ngdoc service
 * @name transcend.job.service:Job
 *
 * @description
 The 'Job' directive provides methods to set up reporting Jobs.
 *

 *
 * @requires $resource
 * @requires jobConfig
 * @requires $window
 *
 */
/**
 * @ngdoc service
 * @name transcend.job.directive:jobList
 *
 * @description
 * The 'jobList' directive allows the viewing, and editing of jobs.
 *
 * @requires $filter
 * @requires $interval
 * @requires $q
 * @requires $timeout
 * @requires transcend.core#$notify
 * @requires transcend.job#jobConfig
 * @requires transcend.job#Job
 *
 * @param {object=} api Api object to bind to in order to gain access to directive functionality.
 * @param {object=} columns Mapping of column names to visibility. This setting is used to specify exactly what
 * columns you want shown/hidden.
 * @param {string=} hideColumns Comma separated list of columsn to hide (based on the default column visibilities).
 * @param {string=} tags Tags to query on - when querying the server for a list of jobs.
 * @param {boolean=} autoRefresh Determines whether to periodically poll the server for job list changes.
 *
 * @example
 <example module="app">
 <file name="index.html">
 <div ng-controller="Ctrl as ctrl">
 <job-list columns="ctrl.columns" on-query="ctrl.onQuery(jobs)"  query-params="{appId: ctrl.appConfig.appId, profileId:ctrl.appConfig.profile.id, ignoreOwner: true}"></job-list>
 </div>
 </file>
 <file name="script.js">
 angular.module('app', ['ui.bootstrap', 'transcend.job', 'ngMockE2E'])
 .controller('Ctrl', function() {
     this.columns = {
      job: true,
      status: true,
      appId: false,
      owner: true,
      startDate: true,
      endDate: false,
      elapsedTime: true,
      expires: true,
      action: false,
      comments: false
    };

    this.onQuery = function(jobs){
      console.log('Queried ' + jobs.length + ' jobs.');
    };
  });
 </file>
 <file name="mocks.js">
 angular.module('app')
 .run(function($httpBackend){
   var mocks = [{"recordCount":108,"filePath":"E:\\output\\08ec8274-2d0d-4cfa-aea5-a60bccf6aa18.sdf","id":"08ec8274-2d0d-4cfa-aea5-a60bccf6aa18","appId":"sa","ownerUserName":"TSS\\rgreen","startDate":"2015-07-23T07:19:16.22","endDate":"2015-07-23T07:19:16.767","neverExpires":true,"expirationDate":"2015-08-06T07:19:16.767","description":"Basic Merge of 2 events against 3 routes","message":"Running Analysis...","status":3,"progress":62, "comment":"Test Comment"},{"recordCount":30,"filePath":"E:\\output\\5f5b9087-f8fc-4437-b4b9-6b65ebc26303.sdf","id":"5f5b9087-f8fc-4437-b4b9-6b65ebc26303","appId":"sa","ownerUserName":"TSS\\yluo","startDate":"2015-07-23T07:15:14.373","endDate":"2015-07-23T07:15:14.657","neverExpires":false,"expirationDate":"2015-08-06T07:15:14.657","description":"Advanced Query of 2 events against 1 routes","message":"Analysis complete","status":5,"progress":100},{"recordCount":30,"filePath":"E:\\output\\a66e65f0-9867-4aa0-9b2a-8f79bcd5686a.sdf","id":"a66e65f0-9867-4aa0-9b2a-8f79bcd5686a","appId":"sa","ownerUserName":"TSS\\yluo","startDate":"2015-07-23T07:15:05.813","endDate":"","neverExpires":false,"expirationDate":"2015-08-06T07:15:06.11","description":"Advanced Query of 2 events against 1 routes","message":"Analysis Cancelled","status":6,"progress":32},{"recordCount":0,"filePath":"E:\\output\\65438db6-3284-489b-8da5-1031d5a854c8.sdf","id":"65438db6-3284-489b-8da5-1031d5a854c8","appId":"sa","ownerUserName":"siteadmin","startDate":"2015-07-23T05:07:05.483","endDate":"2015-07-23T05:07:07.747","neverExpires":false,"expirationDate":"2015-08-06T05:07:07.747","description":"Advanced Query of 2 events against 1 routes","message":"Queried selected route ids","errorMessage":"Failed to run bulk analysis for route list '025A...025A', Details: { Failed to analyze event layers for route id '025A', Details: { Event operator is invalid for LRSE_ACCESS :  } }","status":7,"progress":5},{"recordCount":0,"filePath":"E:\\output\\978e964d-ef85-49a7-8690-44f4623bcdda.sdf","id":"978e964d-ef85-49a7-8690-44f4623bcdda","appId":"sa","ownerUserName":"siteadmin","startDate":"2015-07-23T05:05:35.137","endDate":"2015-07-23T05:05:39.78","neverExpires":false,"expirationDate":"2015-08-06T05:05:39.78","description":"Advanced Query of 2 events against 2 routes","message":"Queried selected route ids","errorMessage":"Failed to run bulk analysis for route list '025A...025A_DEC', Details: { Failed to analyze event layers for route id '025A', Details: { Event operator is invalid for LRSE_ACCESS :  } }","status":7,"progress":5},{"recordCount":59,"filePath":"E:\\output\\25e82277-144a-462b-8a4e-bbccb4081a57.sdf","id":"25e82277-144a-462b-8a4e-bbccb4081a57","appId":"sa","ownerUserName":"siteadmin","startDate":"2015-07-23T04:54:22.293","endDate":"2015-07-23T04:54:24.62","neverExpires":false,"expirationDate":"","description":"Advanced Query of 2 events against 2 routes","message":"Analysis complete","status":5,"progress":100}];

   // Mock our HTTP calls.
   $httpBackend.whenGET(/api\/job\?/)
    .respond(angular.copy(mocks));
 })
 </file>
 </example>
 */
