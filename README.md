# Job Module
The "Job" module provides a set of functionality to view and interact with jobs that have been ran. Jobs are typically
long running processes that process data, export images, etc.

## Installation
The easiest way to use this, and other Transcend libraries, is to use [Bower.js](http://bower.io/). Run the following
commands to install this package:

```
bower install https://bitbucket.org/transcend/bower-job.git
```

Other options:

* Download the code at [http://code.tsstools.com/bower-job/get/master.zip](http://code.tsstools.com/bower-job/get/master.zip)
* View the repository at [http://code.tsstools.com/bower-job](http://code.tsstools.com/bower-job)

When you have pulled down the code and included the files (and dependency files), simply add the module as a
dependency to your application:

```
angular.module('myModule', ['transcend.job']);
```

## To Do
- Implement change job expiration.